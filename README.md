# Purpose
This repository is a container to test meshes generated with HOPR
( [www.hopr-project.org](www.hopr-project.org) ) and used for testing fluxo 
( [https://github.com/project-fluxo/fluxo](https://github.com/project-fluxo/fluxo) )

## License
The files in this repository are released under the terms of the 
GNU General Public License v3.0.
For the full license terms see the included license file [LICENSE.md](LICENSE.md).

